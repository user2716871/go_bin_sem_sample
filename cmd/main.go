package main

import (
    "fmt"
    "sync"
)

type BinSemaphore struct {
    ch chan struct{}
}

func NewBinSemaphore() *BinSemaphore {
    return &BinSemaphore{
        ch: make(chan struct{}, 1),
    }
}

func (sem *BinSemaphore) Up() {
    sem.ch <- struct{}{}
}

func (sem *BinSemaphore) Down() {
    <-sem.ch
}

const n = 10

func printEven(in, out *BinSemaphore) {

    for i := 0; i < n; i += 2 {
        fmt.Println(i)
        out.Up()
        in.Down()
    }
}

func printOdd(in, out *BinSemaphore) {

    for i := 1; i < n; i += 2 {
        in.Down()
        fmt.Println(i)
        out.Up()
    }
}

func main() {

    var wg = sync.WaitGroup{}

    var (
        evenRoutineAllows = NewBinSemaphore()
        oddRoutineAllows  = NewBinSemaphore()
    )

    wg.Add(1)
    go func() {
        printEven(oddRoutineAllows, evenRoutineAllows)
        wg.Done()
    }()

    wg.Add(1)
    go func() {
        printOdd(evenRoutineAllows, oddRoutineAllows)
        wg.Done()
    }()

    wg.Wait()
}
